#include "VkApplication.hpp"

VkApplication::VkApplication() {
    // Every struct in Vulkan has a sType, which MUST BE SET
    // The sType defines the structure type.
    // In Vulkan, on the low-memory, it can happen, that a struct is given as a void*.
    // Vulkan then uses the sType to identify the struct.
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    // pNext is used for extensions
    applicationInfo.pNext = nullptr;
    // The application name
    applicationInfo.pApplicationName = "Testing";
    // The application version
    applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 0, 0);
    // The engine name
    applicationInfo.pEngineName = "Testing - Engine";
    // The engine version
    applicationInfo.engineVersion = VK_MAKE_VERSION(0, 0, 0);
    // The Vulkan API Version
    // 0 == ignore
    // VK_API_VERSION_1_0 == VK1.0 [first release version / RC1]
    // VK_API_VERSION_1_1 == VK1.1
    applicationInfo.apiVersion = VK_API_VERSION_1_0;

    // sType to identify struct
    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    // pNext is used for extensions
    instanceCreateInfo.pNext = nullptr;
    // flags are reserved for the future, 0 = disabled / ignore
    instanceCreateInfo.flags = 0;
    // pointer to our application info
    instanceCreateInfo.pApplicationInfo = &applicationInfo;
    // Layers are executed between single steps in Vulkan
    // we use no layers
    instanceCreateInfo.enabledLayerCount = 0;
    // null value, since no layers
    instanceCreateInfo.ppEnabledLayerNames = nullptr;
    // Extensions extend Vulkan
    // we use no extensions
    instanceCreateInfo.enabledExtensionCount = 0;
    // null value, since no extensions
    instanceCreateInfo.ppEnabledExtensionNames = nullptr;

    createInstance();
    physicalDevices();
}

void VkApplication::createInstance() {
    // Create a Vulkan instance based on the instance create info.
    // Using nullptr for the allocator tells Vulkan to use it's own.
    VkResult result = vkCreateInstance(&instanceCreateInfo, nullptr, &instance);
    // Assert the result (must be VK_SUCCESS)
    ASSERT_VK(result)
}

void VkApplication::physicalDevices() {
    // First we want to know how many physical devices are on the host
    // We create a variable therefore:
    uint32_t amountOfPhysicalDevices = 0;
    // Then parse it to the function.
    // By using nullptr as the third argument, the function will fill the variable for us.
    VkResult result = vkEnumeratePhysicalDevices(instance, &amountOfPhysicalDevices, nullptr);
    ASSERT_VK(result)

    // Next we create the array for the physical devices:
    auto *physicalDevices = new VkPhysicalDevice[amountOfPhysicalDevices];
    // and parse it to the function.
    // This now will fill the array for us.
    result = vkEnumeratePhysicalDevices(instance, &amountOfPhysicalDevices, physicalDevices);
    ASSERT_VK(result)

    std::cout << "Found " << amountOfPhysicalDevices << " physical device(s)." << std::endl;
    for(uint32_t i = 0; i < amountOfPhysicalDevices; i++) {
//        VkUtil::printPhysicalDeviceInfo(physicalDevices[i]);
    }
}

VkApplication::~VkApplication() {

}
