#ifndef TESTING_VKAPPLICATION_HPP
#define TESTING_VKAPPLICATION_HPP

#include <iostream>
#include <vulkan/vulkan.h>
#include "VkAssert.hpp"
#include "VkPrintPhysicalDeviceInfo.hpp"

class VkApplication {
    // The application info provides basic information about the application.
    VkApplicationInfo applicationInfo;
    // The instance create info provides additional information, including the application info, for vulkan.
    VkInstanceCreateInfo instanceCreateInfo;
    // The instance of Vulkan.
    VkInstance instance;

public:
    VkApplication();
    ~VkApplication();

private:
    void createInstance();
    void physicalDevices();
};


#endif //TESTING_VKAPPLICATION_HPP
