#include "SplitPath.hpp"

std::vector<std::string> splitPath(std::string &str, std::set<char> delimiters) {
    std::vector<std::string> result;

    char const *pch = str.c_str();
    char const *start = pch;
    for (; *pch; ++pch) {
        if (delimiters.find(*pch) != delimiters.end()) {
            if (start != pch) {
                std::string _str(start, pch);
                result.push_back(_str);
            } else {
                result.emplace_back("");
            }
            start = pch + 1;
        }
    }
    result.emplace_back(start);

    return result;
}